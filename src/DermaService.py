# -*- coding: utf-8 -*-

import generated.DermaService_pb2_grpc as rpc
import generated.DermaService_pb2 as service


from PIL import Image
import io
import os
import grpc
from os.path import dirname, abspath


import sys
import json

from Analyzer import Analyzer

class DermaService(rpc.DermaServiceServicer):  # inheriting here from the protobuf rpc file which is generated

    def __init__(self):
        self.context = None
        self.analyzer = Analyzer()


    def analyzeImage(self, grpc_request, grpc_context):
        self.context = grpc_context
        pil_image = Image.open(io.BytesIO(grpc_request.image))
        code, confidences = self.analyzer.run(pil_image)
        result = service.AnalyzeResponse(
            code = code,
            confidences = confidences
        )
        return result









