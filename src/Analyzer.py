import threading
import onnxruntime
import numpy as np

onnxruntime.set_default_logger_severity(3)

class Analyzer:
    def __init__(self):
        # For preventing secondary call on working model
        self.lock = threading.Lock()
        path = "model.onnx"
        self.session = onnxruntime.InferenceSession(path)
        self.session.get_modelmeta()
        self.first_input_name = self.session.get_inputs()[0].name


    def run(self,im):
        self.lock.acquire()
        try:
            return self.analyze(im)

        finally:
            self.lock.release()


    def analyze(self,pil):
        numpy_im = self.img2numpy(pil,224)
        outputs = self.session.run(None, {self.first_input_name: numpy_im})
        probs  = self.softmax(outputs[0][0]) #.tolist()

        #print(w)
        return np.argmax(probs), probs.tolist()

    def softmax(self,x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum(axis=0)

    def img2numpy(self, pil, img_size):
        batch = []
        resized_pil = pil.resize((img_size, img_size))
        np_im = np.array(resized_pil) / 255
        normalized = self.normalize(np_im)
        swapped_dim = np.transpose(normalized, (2, 0, 1))
        batch.append(swapped_dim)
        return np.array(batch).astype(np.float32)

    def normalize(self,np_im):
        mean = [0.485, 0.456, 0.406]
        without_mean = np_im - mean
        std = [0.229, 0.224, 0.225]
        normalized = without_mean / std
        return normalized

            

    