# -*- coding: utf-8 -*-

import os
import sys
import configparser

currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, currentdir + os.sep + "generated")

from Container import Container

if __name__ == '__main__':
    #config = configparser.ConfigParser()
    #config.read(currentdir + os.sep + "config.ini")
    server = Container(os.getenv('port', 8085))
    server.start(forever=True)