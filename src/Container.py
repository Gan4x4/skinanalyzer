from concurrent import futures
import grpc

import generated.DermaService_pb2_grpc as rpc
from DermaService import DermaService

import time
class Container:

    def __init__(self, port=50051):
        self.port = port
        self.grpcServer = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        rpc.add_DermaServiceServicer_to_server(DermaService(), self.grpcServer)
        print(f'Starting server. Listening at {self.port}...')
        self.grpcServer.add_insecure_port(f'[::]:{self.port}')

    def start(self, forever = False):
        self.grpcServer.start()       
        print("GrpcServer started")
        if forever:
            self.grpcServer.wait_for_termination()

    def stop(self):
        # TODO stop it in right way
        self.grpcServer.stop(None) # 1 sec, Added after unittest broke in docker
        self.grpcServer.wait_for_termination()
        time.sleep(2)
        print("GrpcServer stopped")
