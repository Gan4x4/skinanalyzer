#!/bin/bash

python3 -m venv venv

./venv/bin/pip3 install --upgrade pip
./venv/bin/pip install onnxruntime
./venv/bin/pip3 install -r src/req.txt


./grpc.sh

./test.sh
