# -*- coding: utf-8 -*-

import unittest
import os
import json
import time
import grpc

from test.GrpcClient import GrpcClient
from .context import currentdir, parentdir


class TestGrpc(unittest.TestCase):
    PORT = 8085
    SERVER_START_DELAY = 5  # sec

    @classmethod
    def setUpClass(cls):
        cls.datadir = currentdir + os.sep + "data"+ os.sep


    def test__false(self):
        filename = self.datadir + "psoriaz/Img_185.jpg"
        start = time.perf_counter()
        while (time.perf_counter() - start) < self.SERVER_START_DELAY:
            try:
                client = GrpcClient(self.PORT)
                code, confidence = client.upload(filename)
                self.assertEqual(0, int(code)) # zero is dummy value
                return True
            except grpc.RpcError:
                # Wait for server
                time.sleep(0.1)

        self.fail(f"Server not started in {self.SERVER_START_DELAY} sec.")


if __name__ == '__main__':
    unittest.main()
