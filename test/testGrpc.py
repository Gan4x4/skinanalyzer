# -*- coding: utf-8 -*-

import unittest
import os
import configparser
import json
import glob

from src.Container import Container
from test.GrpcClient import GrpcClient
from .context import currentdir, parentdir


class TestGrpc(unittest.TestCase):
    PORT = 50051

    @classmethod
    def setUpClass(cls):

        cls.server = Container(cls.PORT)
        cls.server.start()
        cls.client = GrpcClient(cls.PORT)
        cls.datadir = currentdir + os.sep + "data"+ os.sep

    @classmethod
    def tearDownClass(cls):
        cls.server.stop()

    #@unittest.skip("demonstrating skipping")

# ================ Analyze one image ===============================================

    def test_true(self):
        filename = self.datadir + "psoriaz/Img_185.jpg"
        code, confidence = self.client.upload(filename)
        self.assertEqual( 1, int(code)) # fake



if __name__ == '__main__':
    unittest.main()
