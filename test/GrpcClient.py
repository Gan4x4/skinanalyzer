# -*- coding: utf-8 -*-

import grpc

from src.generated import DermaService_pb2 as service
from src.generated import DermaService_pb2_grpc as rpc
import json

class GrpcClient:
    def __init__(self, port=50051, ip = 'localhost'):
        self.port = port
        #channel = grpc.insecure_channel(f'localhost:{self.port}')
        channel = grpc.insecure_channel(f'{ip}:{self.port}')
        self.stub = rpc.DermaServiceStub(channel)

    def upload(self, filename):
        file = open(filename, 'rb')
        jpeg = file.read()
        file.close()
        code, confidences = self.analyzeImage(jpeg)
        return code, confidences

    def analyzeImage(self, jpeg):
        request = service.AnalyzeImageRequest(image=jpeg)
        response = self.stub.analyzeImage(request)
        return response.code, response.confidences

