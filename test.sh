#!/bin/bash

./stop.sh

docker-compose up --build -d

# Run test outside container

./venv/bin/python -m unittest test/testGrpc.py -v

# but one test should perform smoke call to container

./venv/bin/python -m unittest test/testInDocker.py -v

./stop.sh

