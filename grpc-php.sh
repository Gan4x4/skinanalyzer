#!/bin/bash

# Before using this file, grpc modules for php include grpc_php_plugin must be installed and compiled :(

mkdir -p ./php-generated

protoc --php_out=./php-generated --grpc_out=./php-generated --plugin=protoc-gen-grpc=grpc_php_plugin DermaService.proto
#protoc --php_out=./php-generated FMBService.proto